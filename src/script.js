/*## Теоретичне питання

Наведіть кілька прикладів, коли доречно використовувати 
в коді конструкцію `try...catch`.
Відповідь:
- прі виконанні операцій з базами даних, мережевих операцій, роботи з файлами;
- коли потрібно повідомити користувача про помилки, що виникли під час виконання програми;
- коли потрібно забезпечити виконання коду, навіть у випадку виникнення помилок.


*/
const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  
  function listCreation(books) {
    // шукаємо елемент div з id="root"
    const root = document.getElementById("root");
    // створюємо елемент ul
    const ul = document.createElement("ul");
    
    // перевіряємо на наявність властивостей в об'єктах, 
    // якщо всі властивості є то додаємо їх на сторінку.
    books.forEach(book => {
      // перевіряємо, чи містить кожен елемент всі три властивості
      if (typeof book.author === "undefined" || typeof book.name === "undefined" || typeof book.price === "undefined") {
        console.error(`Помилка: в об'єкті відсутня одна з властивостей: ${JSON.stringify(book)}`);
        return;
      }
      // створюємо елемент li
      const li = document.createElement("li");
      // додаємо властивості об'єкту до елементу li
      li.innerHTML = `Автор: ${book.author}<br>Назва: ${book.name}<br>Ціна: ${book.price}<br><br>`;
      // додаємо елемент li в кінець елементу ul
      ul.append(li);
    });
    // додаємо елемент ul до елемента div з id="root"
    root.append(ul);
  }
  
  try {  
    listCreation(books);
  
  } catch (error) {  
    console.log(`Помилка  ${error.name}: ${error.message}`);
  }
  